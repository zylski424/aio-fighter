
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import java.awt.GridLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;


import javax.swing.JTextPane;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;


public class ScriptGui {

	private JFrame frame;
	private main script;
	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					ScriptGui window = new ScriptGui();
//					window.frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the application.
	 */
	public ScriptGui(main script) {
		initialize();
		this.script = script;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBorder(null);
		frame.getContentPane().add(tabbedPane);
		
			JPanel panel = new JPanel();
			tabbedPane.addTab("Monster", null, panel, null);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{45, 44, 70, 0, 0, 0, 0};
			gbl_panel.rowHeights = new int[]{16, 0, 0, 0, 0, 0, 0, 0, 0};
			gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			
			JLabel lblMonsterList = new JLabel("Monster List");
			GridBagConstraints gbc_lblMonsterList = new GridBagConstraints();
			gbc_lblMonsterList.insets = new Insets(0, 0, 5, 5);
			gbc_lblMonsterList.gridx = 1;
			gbc_lblMonsterList.gridy = 1;
			panel.add(lblMonsterList, gbc_lblMonsterList);
			
			JLabel lblName = new JLabel("Add: ");
			GridBagConstraints gbc_lblName = new GridBagConstraints();
			gbc_lblName.anchor = GridBagConstraints.WEST;
			gbc_lblName.insets = new Insets(0, 0, 5, 5);
			gbc_lblName.gridx = 3;
			gbc_lblName.gridy = 1;
			panel.add(lblName, gbc_lblName);
			
			JTextField textPane_1 = new JTextField();
			GridBagConstraints gbc_textPane_1 = new GridBagConstraints();
			gbc_textPane_1.fill = GridBagConstraints.HORIZONTAL;
			gbc_textPane_1.anchor = GridBagConstraints.NORTH;
			gbc_textPane_1.insets = new Insets(0, 0, 5, 5);
			gbc_textPane_1.gridx = 4;
			gbc_textPane_1.gridy = 1;
			panel.add(textPane_1, gbc_textPane_1);
			
			JTextPane textPane_2 = new JTextPane();
			textPane_2.setEditable(false);
			GridBagConstraints gbc_textPane_2 = new GridBagConstraints();
			gbc_textPane_2.gridwidth = 2;
			gbc_textPane_2.gridheight = 5;
			gbc_textPane_2.insets = new Insets(0, 0, 5, 5);
			gbc_textPane_2.fill = GridBagConstraints.BOTH;
			gbc_textPane_2.gridx = 1;
			gbc_textPane_2.gridy = 2;
			panel.add(textPane_2, gbc_textPane_2);
			
			
			JButton btnAddMonster = new JButton("Add Monster");
			
			GridBagConstraints gbc_btnAddMonster = new GridBagConstraints();
			gbc_btnAddMonster.insets = new Insets(0, 0, 5, 5);
			gbc_btnAddMonster.gridx = 4;
			gbc_btnAddMonster.gridy = 2;
			panel.add(btnAddMonster, gbc_btnAddMonster);
			
			btnAddMonster.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					textPane_2.setText(textPane_2.getText()+textPane_1.getText()+"\n");
					textPane_1.setText("");
				}
			});
			
			JButton btnSave = new JButton("Save");
			
			GridBagConstraints gbc_btnSave = new GridBagConstraints();
			gbc_btnSave.fill = GridBagConstraints.HORIZONTAL;
			gbc_btnSave.insets = new Insets(0, 0, 5, 5);
			gbc_btnSave.gridx = 4;
			gbc_btnSave.gridy = 4;
			panel.add(btnSave, gbc_btnSave);
			
		
			
			JLabel label_1 = new JLabel(" ");
			GridBagConstraints gbc_label_1 = new GridBagConstraints();
			gbc_label_1.insets = new Insets(0, 0, 5, 5);
			gbc_label_1.gridx = 0;
			gbc_label_1.gridy = 5;
			panel.add(label_1, gbc_label_1);
			
			JButton btnLoad = new JButton("Load");
			
			GridBagConstraints gbc_btnLoad = new GridBagConstraints();
			gbc_btnLoad.fill = GridBagConstraints.HORIZONTAL;
			gbc_btnLoad.insets = new Insets(0, 0, 5, 5);
			gbc_btnLoad.gridx = 4;
			gbc_btnLoad.gridy = 5;
			panel.add(btnLoad, gbc_btnLoad);
			
			
			JButton btnStart = new JButton("Start");
			
			GridBagConstraints gbc_btnStart = new GridBagConstraints();
			gbc_btnStart.insets = new Insets(0, 0, 5, 5);
			gbc_btnStart.fill = GridBagConstraints.VERTICAL;
			gbc_btnStart.gridx = 4;
			gbc_btnStart.gridy = 6;
			panel.add(btnStart, gbc_btnStart);
			
			
			
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Loot", null, panel_2, null);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_2.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_2.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_2.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_2.setLayout(gbl_panel_2);
		
		JLabel lblLootList = new JLabel("Loot List:");
		lblLootList.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblLootList = new GridBagConstraints();
		gbc_lblLootList.insets = new Insets(0, 0, 5, 5);
		gbc_lblLootList.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblLootList.gridx = 2;
		gbc_lblLootList.gridy = 1;
		panel_2.add(lblLootList, gbc_lblLootList);
		
		JLabel lblNewLabel = new JLabel("Add Item: ");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 5;
		gbc_lblNewLabel.gridy = 1;
		panel_2.add(lblNewLabel, gbc_lblNewLabel);
		
		JTextPane textPane_4 = new JTextPane();
		GridBagConstraints gbc_textPane_4 = new GridBagConstraints();
		gbc_textPane_4.fill = GridBagConstraints.HORIZONTAL;
		gbc_textPane_4.insets = new Insets(0, 0, 5, 5);
		gbc_textPane_4.gridx = 6;
		gbc_textPane_4.gridy = 1;
		panel_2.add(textPane_4, gbc_textPane_4);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridheight = 7;
		gbc_scrollPane.gridwidth = 3;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.gridx = 1;
		gbc_scrollPane.gridy = 2;
		panel_2.add(scrollPane, gbc_scrollPane);
		
		JTextPane textPane_3 = new JTextPane();
		scrollPane.setViewportView(textPane_3);
		textPane_3.setEditable(false);
		
		JButton btnAdd = new JButton("Add");
		
		GridBagConstraints gbc_btnAdd = new GridBagConstraints();
		gbc_btnAdd.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnAdd.insets = new Insets(0, 0, 5, 5);
		gbc_btnAdd.gridx = 6;
		gbc_btnAdd.gridy = 2;
		panel_2.add(btnAdd, gbc_btnAdd);
		
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textPane_3.setText(textPane_3.getText()+textPane_4.getText()+"\n");
				textPane_4.setText("");
			}
		});
		
		JLabel lblAutoLoot = new JLabel("Auto Loot: ");
		GridBagConstraints gbc_lblAutoLoot = new GridBagConstraints();
		gbc_lblAutoLoot.insets = new Insets(0, 0, 5, 5);
		gbc_lblAutoLoot.gridx = 5;
		gbc_lblAutoLoot.gridy = 3;
		panel_2.add(lblAutoLoot, gbc_lblAutoLoot);
		
		JTextPane textPane_5 = new JTextPane();
		textPane_5.setText("1000");
		GridBagConstraints gbc_textPane_5 = new GridBagConstraints();
		gbc_textPane_5.insets = new Insets(0, 0, 5, 5);
		gbc_textPane_5.fill = GridBagConstraints.BOTH;
		gbc_textPane_5.gridx = 6;
		gbc_textPane_5.gridy = 3;
		panel_2.add(textPane_5, gbc_textPane_5);
		
		JLabel label_2 = new JLabel("     ");
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.insets = new Insets(0, 0, 5, 5);
		gbc_label_2.gridx = 0;
		gbc_label_2.gridy = 4;
		panel_2.add(label_2, gbc_label_2);
		
		JRadioButton rdbtnAutoLoot = new JRadioButton("Auto Loot");
		rdbtnAutoLoot.setSelected(true);
		GridBagConstraints gbc_rdbtnAutoLoot = new GridBagConstraints();
		gbc_rdbtnAutoLoot.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnAutoLoot.gridx = 6;
		gbc_rdbtnAutoLoot.gridy = 4;
		panel_2.add(rdbtnAutoLoot, gbc_rdbtnAutoLoot);
		
		JRadioButton rdbtnLootArrows = new JRadioButton("Loot Arrows");
		GridBagConstraints gbc_rdbtnLootArrows = new GridBagConstraints();
		gbc_rdbtnLootArrows.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnLootArrows.gridx = 6;
		gbc_rdbtnLootArrows.gridy = 5;
		panel_2.add(rdbtnLootArrows, gbc_rdbtnLootArrows);
		
		JLabel label = new JLabel("");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 4;
		gbc_label.gridy = 6;
		panel_2.add(label, gbc_label);
		
		JLabel label_4 = new JLabel(" ");
		GridBagConstraints gbc_label_4 = new GridBagConstraints();
		gbc_label_4.insets = new Insets(0, 0, 5, 5);
		gbc_label_4.gridx = 7;
		gbc_label_4.gridy = 8;
		panel_2.add(label_4, gbc_label_4);
		
		JLabel label_3 = new JLabel(" ");
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.insets = new Insets(0, 0, 0, 5);
		gbc_label_3.gridx = 1;
		gbc_label_3.gridy = 9;
		panel_2.add(label_3, gbc_label_3);
		
		JLabel label_5 = new JLabel("              ");
		GridBagConstraints gbc_label_5 = new GridBagConstraints();
		gbc_label_5.insets = new Insets(0, 0, 0, 5);
		gbc_label_5.gridx = 3;
		gbc_label_5.gridy = 9;
		panel_2.add(label_5, gbc_label_5);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Health", null, panel_1, null);
		SimpleAttributeSet center = new SimpleAttributeSet();
		StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
		

		
		
		
		textPane_4.getStyledDocument().setParagraphAttributes(0, textPane_4.getStyledDocument().getLength(), center, true);
		textPane_3.getStyledDocument().setParagraphAttributes(0, textPane_3.getStyledDocument().getLength(), center, true);
		textPane_5.getStyledDocument().setParagraphAttributes(0, textPane_5.getStyledDocument().getLength(), center, true);
		textPane_2.getStyledDocument().setParagraphAttributes(0, textPane_2.getStyledDocument().getLength(), center, true);
		
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{70, 94, 190, 0};
		gbl_panel_1.rowHeights = new int[]{38, 16, 0, 0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JLabel lblPercentToEat = new JLabel("Percent to eat: ");
		GridBagConstraints gbc_lblPercentToEat = new GridBagConstraints();
		gbc_lblPercentToEat.anchor = GridBagConstraints.WEST;
		gbc_lblPercentToEat.insets = new Insets(0, 0, 5, 5);
		gbc_lblPercentToEat.gridx = 1;
		gbc_lblPercentToEat.gridy = 0;
		panel_1.add(lblPercentToEat, gbc_lblPercentToEat);
		
		JSlider slider = new JSlider();
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setMinorTickSpacing(10);
		GridBagConstraints gbc_slider = new GridBagConstraints();
		gbc_slider.anchor = GridBagConstraints.NORTHWEST;
		gbc_slider.insets = new Insets(0, 0, 5, 0);
		gbc_slider.gridx = 2;
		gbc_slider.gridy = 0;
		panel_1.add(slider, gbc_slider);
		
		JLabel lblEatThreshold = new JLabel("Eat Threshold: ");
		GridBagConstraints gbc_lblEatThreshold = new GridBagConstraints();
		gbc_lblEatThreshold.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblEatThreshold.insets = new Insets(0, 0, 5, 5);
		gbc_lblEatThreshold.gridx = 1;
		gbc_lblEatThreshold.gridy = 1;
		panel_1.add(lblEatThreshold, gbc_lblEatThreshold);
		
		JTextPane textPane = new JTextPane();
		textPane.setText("3");
		GridBagConstraints gbc_textPane = new GridBagConstraints();
		gbc_textPane.insets = new Insets(0, 0, 5, 0);
		gbc_textPane.anchor = GridBagConstraints.NORTH;
		gbc_textPane.fill = GridBagConstraints.HORIZONTAL;
		gbc_textPane.gridx = 2;
		gbc_textPane.gridy = 1;
		panel_1.add(textPane, gbc_textPane);
		
		JLabel lblFoodName = new JLabel("Food Name: ");
		GridBagConstraints gbc_lblFoodName = new GridBagConstraints();
		gbc_lblFoodName.insets = new Insets(0, 0, 5, 5);
		gbc_lblFoodName.gridx = 1;
		gbc_lblFoodName.gridy = 2;
		panel_1.add(lblFoodName, gbc_lblFoodName);
		
		JTextPane textPane_6 = new JTextPane();
		GridBagConstraints gbc_textPane_6 = new GridBagConstraints();
		gbc_textPane_6.insets = new Insets(0, 0, 5, 0);
		gbc_textPane_6.fill = GridBagConstraints.BOTH;
		gbc_textPane_6.gridx = 2;
		gbc_textPane_6.gridy = 2;
		panel_1.add(textPane_6, gbc_textPane_6);
		
		JRadioButton rdbtnAutoEat = new JRadioButton("Auto Eat");
		rdbtnAutoEat.setSelected(true);
		GridBagConstraints gbc_rdbtnAutoEat = new GridBagConstraints();
		gbc_rdbtnAutoEat.gridx = 2;
		gbc_rdbtnAutoEat.gridy = 3;
		panel_1.add(rdbtnAutoEat, gbc_rdbtnAutoEat);
		
		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("Combat / Misc", null, panel_3, null);
		GridBagLayout gbl_panel_3 = new GridBagLayout();
		gbl_panel_3.columnWidths = new int[]{0, 0, 0, 0};
		gbl_panel_3.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gbl_panel_3.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_3.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_3.setLayout(gbl_panel_3);
		
		JLabel lblCombatStyle = new JLabel("Combat Style (NO): ");
		GridBagConstraints gbc_lblCombatStyle = new GridBagConstraints();
		gbc_lblCombatStyle.anchor = GridBagConstraints.EAST;
		gbc_lblCombatStyle.insets = new Insets(0, 0, 5, 5);
		gbc_lblCombatStyle.gridx = 1;
		gbc_lblCombatStyle.gridy = 1;
		panel_3.add(lblCombatStyle, gbc_lblCombatStyle);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Attack", "Strength", "Defence", "Balanced"}));
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 5, 0);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 2;
		gbc_comboBox.gridy = 1;
		panel_3.add(comboBox, gbc_comboBox);
		
		JLabel lblOpenDoors = new JLabel("Open Doors (BETA):");
		GridBagConstraints gbc_lblOpenDoors = new GridBagConstraints();
		gbc_lblOpenDoors.insets = new Insets(0, 0, 5, 5);
		gbc_lblOpenDoors.gridx = 1;
		gbc_lblOpenDoors.gridy = 2;
		panel_3.add(lblOpenDoors, gbc_lblOpenDoors);
		
		JRadioButton rButtonDoors = new JRadioButton("");
		GridBagConstraints gbc_radioButton = new GridBagConstraints();
		gbc_radioButton.insets = new Insets(0, 0, 5, 0);
		gbc_radioButton.gridx = 2;
		gbc_radioButton.gridy = 2;
		panel_3.add(rButtonDoors, gbc_radioButton);
		
		JLabel lblNewLabel_1 = new JLabel("Bury Bones (Big only):");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 1;
		gbc_lblNewLabel_1.gridy = 3;
		panel_3.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		JRadioButton radioButton = new JRadioButton("");
		GridBagConstraints gbc_radioButton_2 = new GridBagConstraints();
		gbc_radioButton_2.insets = new Insets(0, 0, 5, 0);
		gbc_radioButton_2.gridx = 2;
		gbc_radioButton_2.gridy = 3;
		panel_3.add(radioButton, gbc_radioButton_2);
		
		JLabel lblWeaponSpecial = new JLabel("Weapon Special: ");
		GridBagConstraints gbc_lblWeaponSpecial = new GridBagConstraints();
		gbc_lblWeaponSpecial.insets = new Insets(0, 0, 0, 5);
		gbc_lblWeaponSpecial.gridx = 1;
		gbc_lblWeaponSpecial.gridy = 4;
		panel_3.add(lblWeaponSpecial, gbc_lblWeaponSpecial);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("");
		GridBagConstraints gbc_rdbtnNewRadioButton = new GridBagConstraints();
		gbc_rdbtnNewRadioButton.gridx = 2;
		gbc_rdbtnNewRadioButton.gridy = 4;
		panel_3.add(rdbtnNewRadioButton, gbc_rdbtnNewRadioButton);
		
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				script.loot = Integer.parseInt(textPane_5.getText());
				script.MONSTER = textPane_2.getText();
				script.LOOT_GUI = textPane_3.getText();
				script.arrow = rdbtnLootArrows.isSelected();
				script.autoLoot = rdbtnAutoLoot.isSelected();
				script.threshold = Integer.parseInt(textPane.getText());
				script.healthPercent = slider.getValue();
				script.autoEat = rdbtnAutoEat.isSelected();
				script.FOOD_NAME = textPane_6.getText();
				script.autoCheckDoors = rButtonDoors.isSelected();
				script.special = rdbtnNewRadioButton.isSelected();
				script.bury = radioButton.isSelected();
				script.start = true;
				frame.setVisible(false);

			}
		});
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				if (fileChooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
				  File file = fileChooser.getSelectedFile();
				  String text = "";
				  try {
				        BufferedReader in;
				        in = new BufferedReader(new FileReader(file));
				        String line = in.readLine();
				        while (line != null) {
				        	text += line;
				            line = in.readLine();
				        }
				        in.close();
				    } catch (Exception ex) {
				   
				    }
				  
				  String[] array = text.split(":");
				  textPane_2.setText(array[0].replaceAll(",", "\n"));
				  textPane_3.setText(array[1].replaceAll(",", "\n"));
				  textPane_5.setText(array[2]);
				  if(array[3].equals("1"))
					  rdbtnAutoEat.setSelected(true);
				  else
					  rdbtnAutoEat.setSelected(false);
				  textPane_6.setText(array[4]);
				  if(array[5].equals("1"))
					  rButtonDoors.setSelected(true);
				  else
					  rButtonDoors.setSelected(false);
				}
			}
		});
		
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				if (fileChooser.showSaveDialog(frame) == JFileChooser.APPROVE_OPTION) {
				  File file = fileChooser.getSelectedFile();
				  try {
					BufferedWriter o = new BufferedWriter(new FileWriter(file));
					o.write(textPane_2.getText().replaceAll("\n", ","));
					o.write(":");
					o.write(textPane_3.getText().replaceAll("\n", ","));
					o.write(":");
					o.write(textPane_5.getText());
					o.write(":");
					if(rdbtnAutoEat.isSelected())
						o.write("1");
					else
						o.write("2");
					
					o.write(":");
					o.write(textPane_6.getText());
					o.write(":");
					if(rButtonDoors.isSelected())
						o.write("1");
					else
						o.write("2");
					
					o.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				  // save to file
				}
			}
		});
		frame.setVisible(true);
	}
}

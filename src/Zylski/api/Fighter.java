package Zylski.api;

import org.dreambot.api.methods.MethodContext;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.methods.tabs.Tab;
import org.dreambot.api.wrappers.interactive.Character;
import org.dreambot.api.wrappers.interactive.NPC;

public class Fighter {

	// singleton MethodContext
	private static MethodContext mc;
	private static boolean checkDoors = false;
	private static int FAILED_ATTEMPTS = 0;
	private static int killed = 0;
	
	public static Character currentMonster = null;
	public static Tile lastDeathTile = null;

	/*
	 * This method should only be called once in onStart
	 */
	public static void setMethodContext(MethodContext mc) {
		Fighter.mc = mc;
	}

	public static void setCheckDoors(boolean check) {
		Fighter.checkDoors = check;
	}

	public static boolean fightByName(String[] creature) {
		// Get the monster by passed in name
		if (!mc.getLocalPlayer().isInCombat()) {
			NPC monster = mc.getNpcs().closest(f -> f != null
					&& (!f.isInCombat() || f.isInteracting(mc.getLocalPlayer())) && stringMatch(creature, f.getName()));

			
					
			// If we are not currently in combat and not running at a monster we clicked on
			if (!mc.getLocalPlayer().isInCombat()) {

				if (mc.getLocalPlayer().getCharacterInteractingWithMe() != null
						&& mc.getMap().canReach(mc.getLocalPlayer().getCharacterInteractingWithMe())
								&& mc.getLocalPlayer().getCharacterInteractingWithMe().canAttack()){
					monster = (NPC) mc.getLocalPlayer().getCharacterInteractingWithMe();
				}
				// If there exists the monster
				if (monster != null) {

					if (checkDoors) {
						Helper.preOpenDoors(false);
					}

					// If the monster is on screen, fight it, else walk to it.
					if (mc.getMap().canReach(monster) && monster.isOnScreen()) {

						monster.interact("Attack");
						MethodContext.sleep(100, 500);
						FAILED_ATTEMPTS = 0;

					} else {
						if (FAILED_ATTEMPTS >= 4 && checkDoors) {
							Helper.preOpenDoors(true);
							FAILED_ATTEMPTS = 0;
						}
						Helper.handleOffScreen(monster);
						FAILED_ATTEMPTS++;

					}

					// returns true if the script successfully tried to attack the monster
					return true;

				}
			}
		}
		updateStatistics();

		// Fight was not necessary or it failed
		return false;

	}
	
	public static int getCount()
	{
		return 
	}

	public static void useSpecial() {

		if (mc.getCombat().getSpecialPercentage() == 100 && !mc.getCombat().isSpecialActive()) {
			if (mc.getLocalPlayer().isInCombat()) {
				if (mc.getLocalPlayer().getCharacterInteractingWithMe().getHealthPercent() > 50) {
					if(!mc.getTabs().isOpen(Tab.COMBAT)) {
						mc.getTabs().openWithMouse(Tab.COMBAT);
						MethodContext.sleep(200,400);  
					}
					if(mc.getTabs().isOpen(Tab.COMBAT)) {
						mc.getCombat().toggleSpecialAttack(true);
						MethodContext.sleep(200,800);
						mc.getTabs().openWithMouse(Tab.INVENTORY);
					}
				}
			}
		}
	}

	// This is a helper method for the filter above
	private static boolean stringMatch(String[] array, String match) {
		for (String s : array) {
			if (s.equals(match))
				return true;
		}
		return false;
	}

	// Might be good info later
	public static void updateStatistics() {
		Fighter.currentMonster = mc.getLocalPlayer().getInteractingCharacter();
		if (currentMonster != null)
			Fighter.lastDeathTile = currentMonster.getTile();
	}

}

package Zylski.api;

import java.util.ArrayList;
import java.util.List;

import org.dreambot.api.methods.MethodContext;
import org.dreambot.api.wrappers.items.GroundItem;

import Zylski.api.GrandExchangeApi.GELookupResult;

public class Looter {

	// Variables for auto looting
	private static ArrayList<Integer> idList = new ArrayList<Integer>();
	private static ArrayList<Integer> valueList = new ArrayList<Integer>();
	private static String[] namesToLoot;
	private static int PRICE_TO_LOOT = 0;
	private static boolean checkDoors = false;
	private static boolean doAutoLoot = true;
	private static boolean eatForLoot = true;

	// Singleton MethodContext
	private static MethodContext mc;
	static GrandExchangeApi exchangeApi;

	/*
	 * This method should only be called once in onStart
	 */
	public static void setMethodContext(MethodContext mc) {
		Looter.mc = mc;
		Looter.exchangeApi = new GrandExchangeApi();

	}

	// GETTERS AND SETTERS
	public static void setNamesToLoot(String[] namesToLoot) {
		Looter.namesToLoot = namesToLoot;
	}

	public static void setPriceToLoot(int price) {
		Looter.PRICE_TO_LOOT = price;
	}

	public static void setDoAutoLoot(boolean doAutoLoot) {
		Looter.doAutoLoot = doAutoLoot;
	}
	
	
	// Loot and item by its name
	public static void lootbyName(String lootName) {
		GroundItem loot = mc.getGroundItems().closest(lootName);

		if (loot != null) {
			if (loot.isOnScreen()) {
				if (!mc.getLocalPlayer().isMoving()) {
					loot.interact("Take");
					MethodContext.sleep(300, 600);
				}
			} else {
				mc.getWalking().clickTileOnMinimap(loot.getTile());
				MethodContext.sleep(300, 600);

			}
		}

	}

	// Loot and item by id (THIS ONE ATTEMPTS TO USE THE CHECK FOR DOORS FEATURE)
	public static void lootbyId(int lootId) {
		GroundItem loot = mc.getGroundItems().closest(lootId);

		if (loot != null) {

			if (checkDoors)
				Helper.openDoor(loot);

			if (loot.isOnScreen()) {
				if (!mc.getLocalPlayer().isMoving()) {
					loot.interact("Take");
					MethodContext.sleep(300, 600);
				}
			} else {
				Helper.handleOffScreen(loot);

			}
		}
	}

	// This loot method also checks for doors
	public static void lootbyStringArray(String[] lootString) {
		List<GroundItem> lootArray = mc.getGroundItems().all(lootString);

		for (GroundItem loot : lootArray)
			if (loot != null) {

				if (checkDoors)
					Helper.openDoor(loot);

				if (loot.isOnScreen()) {
					if (!mc.getLocalPlayer().isMoving() && !mc.getLocalPlayer().isInCombat()) {
						loot.interact("Take");
						MethodContext.sleep(300, 600);
					}
				} else {
					Helper.handleOffScreen(loot);

				}
			}

	}

	// Will not pick up coins
	public static void autoLoot() {
		// If we are not eating for loot then our inventory can not be full
		if (!eatForLoot && !mc.getInventory().isFull() && !mc.getLocalPlayer().isInCombat())
			Looter.loot();
		
		// If we are eating for loot, we need to have food to eat or a spot in the inventory
		if (eatForLoot && (HealthManager.hasFood() || !mc.getInventory().isFull()) && !mc.getLocalPlayer().isInCombat())
			Looter.loot();
	}
	
	public static void buryAllBones()
	{
		
		while(mc.getInventory().contains("Big bones") && !mc.getLocalPlayer().isInCombat()) {
			mc.getInventory().get("Big bones").interact("Bury");
			MethodContext.sleep(400,600);
			MethodContext.sleepUntil(() -> !mc.getLocalPlayer().isAnimating(), 3000); 
		}
	}

	// Sorry guys I can explain this later. 
	private static void loot() {
		List<GroundItem> items = mc.getGroundItems().all();
		for (int i = 0; i < items.size(); i++) {
			if (items.get(i).distance() < 10) {

				if (doAutoLoot) {
					if (!idList.contains(items.get(i).getID())) {

						GELookupResult lookupResult = exchangeApi.lookup(items.get(i).getID());

						if (lookupResult != null) {
							MethodContext.log("Price for item: " + items.get(i).getName() + " : " + items.get(i).getID()
									+ " :" + lookupResult.price + " X " + items.get(i).getAmount());
							idList.add(items.get(i).getID());
							valueList.add(lookupResult.price);

							if (lookupResult.price * items.get(i).getAmount() >= PRICE_TO_LOOT) {
								if (checkDoors)
									Helper.openDoor(items.get(i));

								if (mc.getInventory().isFull())
									HealthManager.eatOne();
								if (mc.getMap().canReach(items.get(i)) && items.get(i).isOnScreen()) {
									items.get(i).interact("Take");
									MethodContext.sleep(300, 600);

								} else {
									Helper.handleOffScreen(items.get(i));
								}

							}

						}

					} else {
						if (valueList.get(idList.indexOf(items.get(i).getID()))
								* items.get(i).getAmount() >= PRICE_TO_LOOT) {
							if (checkDoors)
								Helper.openDoor(items.get(i));

							if (mc.getInventory().isFull())
								HealthManager.eatOne();

							if (mc.getMap().canReach(items.get(i)) && items.get(i).isOnScreen()) {
								items.get(i).interact("Take");
								MethodContext.sleep(300, 600);

							} else {
								Helper.handleOffScreen(items.get(i));
							}
						}
					}
				}

				// Not really a part of auto looting, this comes from the list.
				for (String name : namesToLoot) {
					if (items.get(i).getName().equals(name)) {
						if (checkDoors)
							Helper.openDoor(items.get(i));
						
						if (mc.getInventory().isFull())
							HealthManager.eatOne();

						if (mc.getMap().canReach(items.get(i)) && items.get(i).isOnScreen()) {
							items.get(i).interact("Take");
							MethodContext.sleep(300, 600);

						} else {
							Helper.handleOffScreen(items.get(i));
						}
					}
				}

			}
		}
	}

}

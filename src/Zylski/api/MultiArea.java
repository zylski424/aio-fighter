package Zylski.api;

import java.util.ArrayList;

import org.dreambot.api.methods.map.Area;
import org.dreambot.api.methods.map.Tile;
import org.dreambot.api.wrappers.interactive.Entity;

public class MultiArea {

	private ArrayList<Area> areas = new ArrayList<Area>();
	
	public MultiArea()
	{
		
	}
	
	public MultiArea(Area a)
	{
		this.areas.add(a);
	}
	
	public void addArea(Area a)
	{
		this.areas.add(a);
	}
	
	public void addTile(Tile t)
	{
		this.areas.add(new Area(t));
	}
	
	public boolean contains(Entity e)
	{
		for (Area area : areas)
		{
			if(area.contains(e))
				return true;
		}
		return false;
	}
	public boolean contains(int x, int y)
	{
		for (Area area : areas)
		{
			if(area.contains(x,y))
				return true;
		}
		return false;
	}
	public boolean contains(Tile t)
	{
		for (Area area : areas)
		{
			if(area.contains(t))
				return true;
		}
		return false;
	}
}

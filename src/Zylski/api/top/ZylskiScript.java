package Zylski.api.top;

import org.dreambot.api.script.AbstractScript;

import Zylski.api.Fighter;
import Zylski.api.HealthManager;
import Zylski.api.Helper;
import Zylski.api.Looter;


/*
 * By creating an abstract class between the script and Abstract class
 * we can do a lot more things behind the scene that will allow for easier development later
 */
public abstract class ZylskiScript extends AbstractScript{
		
	/*
	 * (non-Javadoc)
	 * @see org.dreambot.api.script.AbstractScript#onStart()
	 * 
	 * Use onStart to set the method context for all static method groups 
	 */
	@Override
	public void onStart() {
		Fighter.setMethodContext(this);
		Looter.setMethodContext(this);
		HealthManager.setMethodContext(this);
		Helper.setMethodContext(this);
	}
}
